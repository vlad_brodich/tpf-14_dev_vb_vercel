import {create} from 'zustand';
import {persist} from 'zustand/middleware';
import { UseFavoritesData } from './store.interfase';

const useFavoritesStore = create<UseFavoritesData>()(
    persist(
    (set)=>({
    characters: [],
    addComment:(id,data)=>{
        set((state)=>({
            characters:state.characters.map((el) => {
                if (el.id === id) {
                    if (el.comments) {
                      return{...el,comments:[data,...el.comments]}  
                    }
                    else return{...el,comments:[data]}
                   
                }
                else return el
            })
        }))
    },    
    delitCharacter:(id)=>{
        set((state)=>({
            characters:state.characters.filter((el)=>(el.id!==id))
        }))
    },
    addCharacter:(newCharacter)=>{
        set((state)=>({
            characters:[newCharacter, ...state.characters]
        }))
    },
    }),
    
    {
        name: 'favorites',
    })
);

export default useFavoritesStore;