interface Comment{
    username: string
    email: string
    text:string
    date:string
}

interface CharacterFavorit extends Character{
    comments?:Comment[]
}

export interface UseFavoritesData{
    characters:CharacterFavorit[],
    delitCharacter:(id:number)=>void
    addCharacter: (newCharacter: Character) => void
    addComment:(id:number,data:Comment)=>void
}