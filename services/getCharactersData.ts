import axios from 'axios';

export async function getCharactersData(page:number) :Promise<{info:info; results:Character[]}>{
    try{
        const res = await axios.get(`https://rickandmortyapi.com/api/character/?page=${page}`)
        return res.data
    }catch(error){
        throw new Error('Failed to fetch data')
    } 
}
