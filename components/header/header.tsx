import Image from 'next/image';
import React from 'react';
import logo from './image/logo_header.svg';
import Link from 'next/link';
import Navigation from '../navigation/navigation';


export default function Header() {
  return (
      <header  className="text-white px-4">
        <nav className="flex content-center m-2 justify-between">
             <Link href='/' className="pr-10 mr-10 w-1/4 min-w-[200px]">
                    <Image
                        src={logo}
                        alt="logo"
                        priority={true}
                        className="object-cover"
                    />
                </Link>
              <Navigation/>
        </nav>
    </header>
  )
}
