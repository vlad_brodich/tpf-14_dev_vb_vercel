import React from 'react';
import CharacterCard from '../character-card/character-card';
import { charactersListProps } from './characters-list.props';
import Link from 'next/link';

export default function CharactersList({ characters }: charactersListProps): JSX.Element {
    if(Array.isArray(characters)){
        return (
            <ul className="grid justify-center gap-4 sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-5 xl:grid-cols-6">
               
                {characters.map((el)=>
                    <li key={el.id}  className="border rounded-lg bg-green-400 p-4 max-w-[230px]">
                        <Link href={`characters/${el.id}`}>
                            <CharacterCard {...el}></CharacterCard>
                        </Link>   
                    </li>)
                }
                  
            </ul>
        )
    }else return <></>

};