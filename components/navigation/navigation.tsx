'use client';

import Link from 'next/link';
import React from 'react';
import { usePathname } from 'next/navigation';

export default function Navigation() {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const pathname = usePathname()

  return (
    <ul className="flex items-center gap-x-3.5">
    <li>
       <Link className={pathname === '/'?"text-blue-300/90 hover:text-blue-100/75":"hover:text-blue-100/75"} href='/'>Home</Link>
   </li>
    <li>
       <Link className={pathname === '/characters'?"text-blue-300/90 hover:text-blue-100/75":"hover:text-blue-100/75"} href='/characters'>Characters</Link>
   </li>
   <li>
       <Link className={pathname === '/favorites'?"text-blue-300/90 hover:text-blue-100/75":"hover:text-blue-100/75"} href='/favorites'>Favorites</Link>
   </li>
</ul>
  )
}
