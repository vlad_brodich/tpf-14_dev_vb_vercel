import Link from "next/link";
import React from "react";
import logo from './image/footer_logo.svg';
import Image from "next/image";
import Navigation from "../navigation/navigation";

export default function Footer():React.JSX.Element{
    return (
        <footer className="bg-black text-white px-4">
            <nav className="flex content-center m-2 ">
                <Link href='/' className="pr-10 mr-10 w-40 border-r-2 border-white">
                    <Image
                        src={logo}
                        alt="logo"
                        priority={true}
                        className="object-cover"
                    />
                </Link>
                <Navigation/>
            </nav>
        </footer>
    )
}