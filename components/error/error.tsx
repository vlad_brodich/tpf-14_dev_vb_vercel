import Link from "next/link";

export default function Error():JSX.Element{
    return(
        <div className="flex justify-center items-center bg-black opacity-50 fixed top-0 right-0 bottom-0 left-0 z-10">
            <div className='text-center'>
                <h2 className="text-red-600 text-3xl m-4 font-bold">Sorry, there was a download error</h2>
                <Link className={"hover:text-blue-400 text-xl"} href='/'>Home</Link>
            </div>    
        </div>
    )
}