export default function Loading():JSX.Element{
    return(
        <div className="flex justify-center bg-black opacity-50 fixed top-0 right-0 bottom-0 left-0 z-10">
            <h2 className='text-white text-xl animate-slide-in-right-infinite '>Loading...</h2>
        </div>
    )
}