import Providers from '@/lib/providers'
import '@/styles/globals.css'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google';
import Footer from '@/components/footer/footer';

import './style/style.css';
import Header from '@/components/header/header';


const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
	title: 'Rick and Morty',
	description: 'Test project front end Rick and Morty'
}

export default function RootLayout({ children }: { children: React.ReactNode }) {
	
	return (
		<html lang="en" data-theme="dark">
			<body className={inter.className}>
				<Providers>
					<div className={'main_page max-w-screen-xl min-h-screen flex flex-col justify-between mx-auto'}>
						<Header></Header>
						<div className=' mx-auto min-w-full'>
							{children}
						</div>
						<Footer></Footer>
					</div>	
				</Providers>
			</body>
		</html>
	)
}
