"use client"
import React from "react";
import { useQuery } from "@tanstack/react-query";
import Image from "next/image";
import { getCharacter } from "@/services";
import Loading from "@/components/loading/loading";
import Error from "@/components/error/error";
import { useRouter } from "next/navigation";
import { Button } from "@/components/ui/button";
import { ChevronLeft } from "@/components/icons";
import useFavoritesStore from "@/store";
import { elIsCharacters } from "@/function";
import star from '../../image/star.svg';

export default function Page({params}:{params:{id:number}}){

    const router = useRouter()
    // Отримання данних зі Store zustand
    // Данні персонажів доданих до Favorirе
    const charactersFavorir = useFavoritesStore((state)=>state.characters);
    // Метод додати персонаж до Favorirе
    const addCharacter = useFavoritesStore((state)=>state.addCharacter);
    // Отримання данних персонажа.
    const{data:character,isLoading,isError}= useQuery({
        queryKey:['character',params.id],
        queryFn: ()=>getCharacter(params.id)
    })
    // Індикатор завантаження.
    if(isLoading){
        return <Loading/>
    }
    // Помилка.
    if(isError){
        return <Error/>
    }
    // Вивід данних.
    return <div className="dorder rounded-lg bg-green-400 w-4/5 my-3 mx-auto p-6">

        <div className='flex justify-between my-2'>

        <Button className='bg-inherit transition-transform hover:bg-inherit hover:-translate-x-1 text-lg' onClick={()=>{
            router.back()
        }}>
            <ChevronLeft></ChevronLeft>
            back
        </Button>

       {elIsCharacters(charactersFavorir,character.id)?
            <div className='border border-green-200 rounded-full p-1'>
                <Image width={40} src={star} alt="star"></Image>
            </div>: 
            <Button className='bg-inherit transition-colors hover:bg-green-200 text-lg border' onClick={()=>{
           addCharacter(character)
        }}>
                    Add to Favorite 
            </Button>}
        </div>

        <div className="relative w-full min-h-[400px] my-2">
            <Image src={character.image} alt={character.name}  fill sizes="300px" className="object-contain " priority={true}/>
        </div>
        <div className= 'px-4 gap-2 text-lg'>
            <h2 className='text-xl'>{character.name}</h2>
            <p className="font-light">Gender: <span className='font-medium'>{character.gender}</span></p>
            <p className="font-light">Type: <span className='font-medium'>{character.type}</span></p>
            <p className="font-light">Status: <span className='font-medium'>{character.status}</span></p>
            <p className="font-light">Species: <span className='font-medium'>{character.species}</span></p>
            <p className="font-light">Location: <span className='font-medium'>{character.location.name}</span></p>
        </div>    
    </div>
}