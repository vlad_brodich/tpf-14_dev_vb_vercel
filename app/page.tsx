import Image from 'next/image';
import img from './image/main_img.svg';
import title_img from './image/rick_and_morty.svg'
import Link from 'next/link';

export default function Home(){
	return (
		<main className="main_page_start">
			<h1 className='mx-auto w-1/3 animate-my-scale-fade-in'>
				<Image className='' src={title_img} alt='rick and morty'></Image>
			</h1>
			<Link href={'/characters'} className='block w-1/3 mx-auto min-w-[200px] animate-my-scale-in '>
				<Image src={img} alt='main_img'></Image>
			</Link>
		</main>
	)
}
