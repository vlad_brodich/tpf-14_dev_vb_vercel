'use client';

import { Button } from '@/components/ui/button';
import useFavoritesStore from '@/store';
import React from 'react';
import СommentForm from '@/components/comment-form/comment-form';
import Image from 'next/image';
import error_logo from '../image/error_img.svg';

export default function Favorites() {

  const charactersFavorir = useFavoritesStore((state)=>state.characters);
  const delitCharacter = useFavoritesStore((state)=>state.delitCharacter);

  return (
    <main className='flex flex-col justify-center mx-auto'>
      <h1 className='w-4/5 mx-auto font-bold text-2xl m-3 text-green-500'>Favorites</h1>
      {charactersFavorir.length === 0?
      <div className='w-4/5 mx-auto flex'>
        <h2 className='text-white font-bold text-3xl m-3'>Unfortunately, you have no saved characters</h2>
        <div>
          <Image src={error_logo} alt='error img'></Image>
        </div>
      </div>:
      <ul className='m-5'>
        {charactersFavorir.map((el) => {
          
          return(
            <li className='bg-green-400 min-w-[300px] m-2 border p-3 rounded-lg' key={el.id}>
              <div className="dorder rounded-lg bg-primary w-1/2 my-0 mx-auto p-4">
                <div className="relative w-full min-h-[400px]">
                    <Image src={el.image} alt={el.name}  fill sizes="300px" className="object-contain " priority={true}/>
                </div>
                 
                <h2 className='text-xl'>{el.name}</h2>
                <p className="font-light">Gender: <span >{el.gender}</span></p>
                <p className="font-light">Type: <span className='font-medium'>{el.type}</span></p>
                <p className="font-light">Status: <span className='font-medium'>{el.status}</span></p>
                <p className="font-light">Species: <span className='font-medium'>{el.species}</span></p>
                <p className="font-light">Location: <span className='font-medium'>{el.location.name}</span></p>
              </div>

              <Button variant={"warning"} onClick={()=>{
                delitCharacter(el.id)
              }}>Delite</Button>
              <ul className='gap-4 w-auto text-center'>
                <li>
                  <СommentForm useId={el.id}></СommentForm>
                </li>
                {el.comments && el.comments.map((e,i) => {
                  return (
                    <li className='rounded p-2 my-2 bg-green-600 text-start px-5' key={i}>
                      <h3>{i+1}. {e.date}</h3>
                      <p className="font-light">Name: <span className='font-medium'>{e.username}</span></p>
                      <p className="font-light">Email: <span className='font-medium'>{e.email}</span></p>
                      <p className="font-light">Comment: <span className='font-medium'>{e.text}</span></p>
                    </li>
                  )
                })}
              </ul>
            
            </li>
          )
        })}
      </ul>
      }
    </main>
  )
}
